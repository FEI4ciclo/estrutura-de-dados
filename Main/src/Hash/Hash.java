package Hash;
/**
 *
 * @author lucas
 */
public class Hash {
    public String[][] tabelaHash;
    int tamanho; //tamanho
    int usados = 0; // usados

    public Hash(int tamanho) {
        this.tamanho = tamanho;
        tabelaHash = new String[tamanho][2];
        for(int i=0; i < tamanho;i++){
            tabelaHash[i][0] = "-1";
        }
    }
    public int geraChave(String chave, String[][] tabelaHash) {
        //funcao recebe chave e calcula o hashcode da string 
        //retorna o valor de code como um inteiro valido dentro do tamanho da hash
        int code  = chave.hashCode();
        code  = code % tamanho ;
        System.out.println(code);
        return code;
    }
    public void cadastroPorChave(String[] chave, String[][] tabelaHash){
        //funçao recebe o um vetor com duas strings usuario e login
        //registra na tabelaHash em suas respectivas posições
        int i = geraChave(chave[0],tabelaHash);
        tabelaHash[i][0] = chave[0]; //coloca o usuario na tabela hash
        tabelaHash[i][1] = chave[1]; //coloca a senha na tabela hash
        System.out.println("cadastrado");
    }
    public boolean buscaChave(String[] chave, String[][] tabelaHash){
        int i = geraChave(chave[0], tabelaHash);
        if(tabelaHash[i][0].equals(-1) || tabelaHash[i][0].isEmpty()){
            System.out.println("usuario nao encontrado");
            return false;
        }else{
            System.out.println();
            return true;
        }
    }
} 