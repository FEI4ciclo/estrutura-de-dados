package LES;
/**
 *
 * @author elias
 */
import main.Jframe.Segunda;
import java.util.Scanner;  // Import the Scanner class
import main.Produto;

public class LES {
    
    Scanner input = new Scanner(System.in);
    Segunda s = new Segunda();
    
    Produto vetor[];
    int n = 0;
    int M;

    public int getN() {
        return n;
    }
    
    public Produto[] getVetor() {
        return vetor;
    }
    
    public LES(int M) {
        this.n = 0;
        this.vetor = new Produto[M];
        this.M = M;
    }
    
    public void Imprime(){
        System.out.println("LES: ");
        System.out.println("nome" +" qtd"+" preco");
        for (int i = 0; i < n; ++i)
            System.out.println(vetor[i].nome+"   " + vetor[i].qtd +"  "+ vetor[i].preco);

        System.out.println();
    }
    
    public int Insere(Produto novo){
        if(n==M)
            return -1;
    
        int i = Busca(novo.getNome());
        //se tem algum na les com o mesmo nome acrescenta quantidade
        if(i != -1){
            ////// JA ESXISTE NA LES
            vetor[i].setQtd(novo.getQtd() + vetor[i].getQtd());
            return i;
        }else{
            for (i = 0; i < n && vetor[i].nome.compareTo(novo.getNome())<=0; ++i);
            // organiza 
            for (int j = n; j > i; --j) {
                vetor[j] = vetor[j-1];
            }
            /////// SE B FOR UMA POSIÇAO NOVA B=K(LES N) CRIA PRODUTO NA POS K
            vetor[i] = novo;
            n++;
            return i;
        }
    }
     
    public int Busca(String nome) {
        //busca valor
        int i = 0;
        for (i = 0; i < n; i++) {
            if(nome.equals(vetor[i].nome.toString())){
                return i;
            }
        }
        return -1;
    }
    
    public boolean Remove(String Nome, int qtd){
        int i = Busca(Nome);
        //se tem algum na les com o mesmo nome subtrai quantidade
        if(i != -1){
            if(vetor[i].getQtd() - qtd == -1){
                for (;i <n ; i++){
                    vetor[i] = vetor[i+1];
                }
                n--;
                System.out.println("Removido da LES");
                return true;
                
            }else if(vetor[i].getQtd() - qtd < -1)
                return false;
            else
                vetor[i].setQtd(vetor[i].getQtd() - qtd);
            
        }        
        return false;
    }
}
