package LES;

import javax.swing.table.AbstractTableModel;
/**
 *
 * @author unifecunha
 */
interface LesPrinter {
    void printL(LES l, AbstractTableModel tabela);
}
