package LES;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import main.Produto;
/**
 *
 * @author unifecunha
 */
public class LesTableModelPrinter implements LesPrinter {
    @Override
    public void printL(LES les, AbstractTableModel tabela) {
        DefaultTableModel model = (DefaultTableModel) tabela;
        model.setRowCount(0);
        Object rowData[] = new Object[4];
        Produto vetor[] = les.getVetor();
        
        for (int i = 0; i < les.getN(); ++i) {
            rowData[0]=  vetor[i].nome;
            rowData[1] = vetor[i].qtd;
            rowData[2] = vetor[i].preco;
            rowData[3] = vetor[i].precoVenda;

            model.addRow(rowData);
        }
    }


    
}
