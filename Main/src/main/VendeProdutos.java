package main;

import Fila.FilaTableModelPrinter;
import LES.LesTableModelPrinter;
import Pilha.PilhaTableModelPrinter;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static main.Jframe.Segunda.fila;
import static main.Jframe.Segunda.les;
import static main.Jframe.Segunda.pilha;
import main.Produto;
/**
 *
 * @author elias
 */
public class VendeProdutos {
    
    public void vende(DefaultTableModel tabela,DefaultTableModel tabela2, DefaultTableModel tabela3, JTextField nome, JTextField qtd, double totalVenda,JTextField venda, JTextField preco){
        
        LesTableModelPrinter PrintLes = new LesTableModelPrinter();
        PilhaTableModelPrinter PrintPilha = new PilhaTableModelPrinter();
        FilaTableModelPrinter PrintFila = new FilaTableModelPrinter();  
        
        
        
        try{
            //remove a quantidade vendida da les
            les.Remove(nome.getText(), Integer.parseInt(qtd.getText()));
            
            //registra na pilha a venda
            Produto novo = new Produto();
            Produto v[] = les.getVetor();
            int pos = les.Busca(nome.getText());
            if(pos != -1){ 
                novo.setNome(nome.getText());
                novo.setQtd(Integer.parseInt(qtd.getText()));
                
                pilha.Insere(novo);
                pilha.Imprime("Venda");
                
                Produto vF[] =  fila.getVetor();
                //checa se condiçao de quantidade minina é satisfeita 
                if(pos != -1 && v[pos].getQtd() < 16 ){
                    //entra na condiçao da Fila de reestoque
                    if(v[pos].getQtd() >= 0){
                        int j=0;
                        for(int k = fila.getI(); k!=fila.getF(); k=(k+1)%fila.getMax()){
                            if(vF[k].getNome().equals(nome.getText())){
                                j=1;
                            }
                        }
                        if(j==0){

                            fila.Insere(v[pos]);
                            fila.Imprime();
                            //atualiza caixa de texto da fila
                            PrintFila.printF(fila,tabela3);
                        }
                    }
                }
                //atualiza as caixas de texto
                PrintPilha.printP(pilha, tabela2, "Venda");
            }
            PrintLes.printL(les,tabela); 
            //Calcula o lucro
        totalVenda = totalVenda + Double.parseDouble(preco.getText())*Integer.parseInt(qtd.getText());
        venda.setText("$" + totalVenda); 
        }catch (NumberFormatException ex){ 
            JOptionPane.showMessageDialog(null, "erro");
        }
           
    }
}
