package main;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static main.Jframe.Primeira.hash;
import static main.Jframe.Primeira.tela2;
/**
 *
 * @author lucas
 */
public class LoginUsuario {
    public void login(JTextField usuario, JTextField senha){
        String u[] = {usuario.getText(),senha.getText()};
        int a = hash.geraChave(u[0], hash.tabelaHash); //acha o index para tabelaHash
        try{ 
            if( !u[1].isEmpty() && hash.tabelaHash[a][1].equals(u[1])){
                //senha foi confirmada
                tela2.setVisible(true);
                System.out.println("\nlogin\n");
            }else{
                //usuario nao encontrado ou senha incorreta
                JOptionPane.showMessageDialog(null, "Credenciais invalidas");
            }
        }catch (Exception ex){ 
            //input invalido
            JOptionPane.showMessageDialog(null, "usuario invalido!");
        }
    }
}
