package main;

import Fila.FilaTableModelPrinter;
import LES.LesTableModelPrinter;
import Pilha.PilhaTableModelPrinter;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import static main.Jframe.Segunda.fila;
import static main.Jframe.Segunda.les;
import static main.Jframe.Segunda.pilha;
/**
 *
 * @author User
 */
public class ReestocaProdutos {
    public double reestoca(AbstractTableModel tabela,AbstractTableModel tabela2,AbstractTableModel tabela3){
        //funçao completa qtd de determindado item na les para 15 e retorna quantos foram adicionados
        LesTableModelPrinter PrintLes = new LesTableModelPrinter();
        PilhaTableModelPrinter PrintPilha = new PilhaTableModelPrinter();
        FilaTableModelPrinter PrintFila = new FilaTableModelPrinter();
        try{
            Produto novo = new Produto();
            //verifica se a fila esta vazia
            if(fila.getI()!=fila.getF()){//se fila nao estiver vazia 
                System.out.println("entrou no reestoca");

                    novo.setNome(fila.primeiro().getNome());
                    novo.setPreco(fila.primeiro().getPreco());
                    int quant = 15 - fila.primeiro().getQtd();
                    novo.setQtd(quant);
                    les.Insere(novo);
                    les.Imprime();
                    
                    pilha.Insere(novo);
                    pilha.Imprime("Reestoca");
                    
                    fila.Imprime();
                    fila.Remove();
                     // atualiza as tabelas
                    PrintLes.printL(les,tabela);
                    PrintPilha.printP(pilha, tabela2, "Reestoca");
                    PrintFila.printF(fila,tabela3);
                    //retorna o total gasto no reestoque
                    return quant;
            }
        }catch (Exception ex){ 
            JOptionPane.showMessageDialog(null, "falha ao reestocar");
        }    
        return 0;
    }
}
