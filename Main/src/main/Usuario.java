package main;
/**
 *
 * @author elias
 */
public class Usuario {
    String login;
    String senha;
    
    Usuario(){
        
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
