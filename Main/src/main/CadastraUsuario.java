package main;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static main.Jframe.Primeira.hash;
/**
 *
 * @author lucas
 */
public class CadastraUsuario {
    public void cadastra(JTextField usuario, JTextField senha){
        String u[] = {usuario.getText(),senha.getText()};
            //se o input não estiver vazio
        if( !u[0].isEmpty() && !u[1].isEmpty()){
            //compara a tabelaHash com a chave referente ao usuario
            if(hash.tabelaHash[hash.geraChave(u[0], hash.tabelaHash)][0].equals(u[0])){
                //tabelaHash[0] ja existe e é igual ao u[0] logo usuario ja foi cadastrado
                JOptionPane.showMessageDialog(null,"Usuário já existe!");
            }else{
                //registra na tabelaHash login e senha
                hash.cadastroPorChave(u, hash.tabelaHash);
            }
        }else{
            JOptionPane.showMessageDialog(null,"Campos obrigatorios estao em branco");
        }
    }
}
