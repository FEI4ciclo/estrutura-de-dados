package main;

import Fila.FilaTableModelPrinter;
import LES.LesTableModelPrinter;
import Pilha.PilhaTableModelPrinter;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import static main.Jframe.Segunda.fila;
import static main.Jframe.Segunda.les;
import static main.Jframe.Segunda.pilha;
/**
 *
 * @author unifecunha
 */
public class CompraProdutos {
    public void compra(JTextField nome,JTextField qtd, JTextField preco,JTextField gasto, AbstractTableModel model,AbstractTableModel model2,AbstractTableModel tabela3,double gastoTotal){
        //declara classes para atualizar paineis de texto e tabelas na interface
        LesTableModelPrinter PrintLes = new LesTableModelPrinter();
        PilhaTableModelPrinter PrintPilha = new PilhaTableModelPrinter();
        FilaTableModelPrinter PrintFila = new FilaTableModelPrinter();   
        
        try{        
            if(nome.getText().isEmpty() || qtd.getText().isEmpty() || preco.getText().isEmpty()){
                //vereifica se os inputs necessarios estao vazios 
                JOptionPane.showMessageDialog(null, "verifique se todos os campos de texto estao prenchidos");
                return;

            }else {   
                Produto novo = new Produto();
                // SE B FOR UMA POSIÇAO NOVA B=K(LES N) CRIA PRODUTO NA POS K
                //registra nome qtd e preço no novo produto
                novo.setNome(nome.getText());
                novo.setQtd(Integer.parseInt(qtd.getText()));
                novo.setPreco(Double.parseDouble(preco.getText()));
                //insere na les e imprime a les ordenada no console 
                les.Insere(novo);
                les.Imprime();
                //registra na pilha a venda do produto e a quantidade vendida
                pilha.Insere(novo);
                pilha.Imprime("Compra"); 
                Produto vF[] = fila.getVetor();
                Produto v[] = les.getVetor();
                //checa se a condiçao de reestoque foi satisfeita ou se a compra
                //ainda nao possui a quantidade minima de produtos
                int pos = les.Busca(nome.getText());
                if(pos != -1 && v[pos].getQtd() <= 15 ){
                    //quantidade menor que a minima
                    if(v[pos].getQtd() >= 0){
                        int j=0;
                        for(int k = fila.getI(); k!=fila.getF(); k=(k+1)%fila.getMax()){
                            if(vF[k].getNome().equals(nome.getText())){
                                j=1;
                            }
                        }
                        if(j==0){

                            fila.Insere(v[pos]);
                            fila.Imprime();
                            //atualiza caixa de texto da fila
                            PrintFila.printF(fila,tabela3);
                        }
                    }
                }
                //quantidade satisfaz a quantidade minima sai da fila de reestoque
                if(pos != -1 && v[pos].getQtd() == 15 )
                    fila.Remove();
                
                gastoTotal = gastoTotal + Double.parseDouble(preco.getText())*Integer.parseInt(qtd.getText());
                gasto.setText("$" + gastoTotal);
            } 
              
        }catch (Exception ex){ 
            JOptionPane.showMessageDialog(null, "produto esgotado ou fora do sistema");
        }
        //atualiza os paineis de texto
        PrintLes.printL(les,model);
        PrintPilha.printP(pilha, model2, "Compra");
        PrintFila.printF(fila,tabela3);      
    } 
}
