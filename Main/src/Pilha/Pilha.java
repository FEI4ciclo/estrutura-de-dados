package Pilha;
/**
 *
 * @author Matheus Silva
 */
import javax.swing.table.DefaultTableModel;
import main.Produto;

public class Pilha {
    public Produto[] prod;  
    public int n;  

    public int getN() {
        return n;
    }

    public Produto[] getProd() {
        return prod;
    }
    
    //Construtor que recebe um capacidade como parâmetro
    public Pilha(int capacidade) {  
        prod = new Produto[(capacidade)];  
        n = 0;  
    }  

    //Método utilizado para adicionar elementos à nossa pilha 
    public int Insere(Produto novo) {  
        if(novo.nome == null)  
            throw new IllegalArgumentException("O produto não pode ser nulo!");  
    
        if(n == prod.length)  
            return -1;  

        prod[n] = new Produto();    
        prod[n] = novo;

        n++; 
        return 1;
    }  

    public void Imprime(String status){
        System.out.println("Pilha: ");
        System.out.println("nome" +" qtd"+" status");
        System.out.println(prod[n-1].nome+"  " + prod[n-1].qtd +"  "+ status);
    }

    //Método utilizado para limpar todo o conteúdo da pilha. 
    public void Limpa() {  
        for(int i = 0; i < n; i++)  
            prod[i] = null;  
        
        n = 0; 
        System.out.println("limpa pilha");
    }
}