package Pilha;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import main.Produto;
/**
 *
 * @author User
 */
public class PilhaTableModelPrinter implements PilhaPrinter{
    @Override
    public void printP(Pilha pilha, AbstractTableModel tabela, String status) {
        DefaultTableModel model = (DefaultTableModel) tabela;        
        Object rowData[] = new Object[3];
        Produto vetor[] = pilha.getProd();

        rowData[0] =  vetor[pilha.getN()-1].getNome();
        rowData[1] = vetor[pilha.getN()-1].getQtd();
        rowData[2] = status;
        System.out.println();
        model.addRow(rowData);
    }
    public void printaVazia(AbstractTableModel tabela){
        DefaultTableModel model = (DefaultTableModel) tabela; 
        model.setRowCount(0);
    }
}
