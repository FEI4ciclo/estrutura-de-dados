package Fila;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import main.Produto;
/**
 *
 * @author User
 */
public class FilaTableModelPrinter implements FilaPrinter{
    public void printF(Fila fila, AbstractTableModel tabela) {
        DefaultTableModel model = (DefaultTableModel) tabela;
        Object rowData[] = new Object[1];
        model.setRowCount(0);
        Produto vetor[] = fila.getVetor();
        int k=0;
        for (k = fila.getI(); k!=fila.getF(); k=(k+1)%fila.getMax()) {
            rowData[0]=  vetor[k].nome;
            
            model.addRow(rowData);
        }
    }
}
