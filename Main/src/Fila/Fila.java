package Fila;

import main.Produto;
/**
 *
 * @author Unifmathsilva
 */
public class Fila {
    public Produto vetor[];	// vetor pra guardar elementos
    public int i;		// elementos da frente
    public int f;		// elementos de tras
    public int max;             // tam max da fila

    public int getI() {
        return i;
    }

    public int getF() {
        return f;
    }
    
    public Produto primeiro(){
        return vetor[i];
    }
    
    public int getMax() {
        return max;
    }
    
    public Produto[] getVetor() {
        return vetor;
    }

    // Construtor da fila
    public Fila(int size){
        vetor = new Produto[size];
        max = size+1;
        i =f=0;
    }

    // remove da fila
    public void Remove(){
        // checa se fila remove o vazio
        if (i == f){
                System.out.println("Fila vazia\n");
                return;
        }
        System.out.println("Removendo " + vetor[i].getNome());
        
        i = (i + 1) % max;
        return;
    }

    // adiciona na fila
    public boolean Insere(Produto item){
        // checa se fila passa do limite
        if( (f+1) % max == i){
            return false;
        }
        vetor[f] = new Produto();
        vetor[f] = item;
        
        f = (f + 1) % max;
        
        return true;
    }
    //printa a fila toda
    public void Imprime(){
        System.out.println("\nFila: ");
        System.out.println("nome");
        int k;
        for (k = i; k!=f; k=(k+1)%max) {
            System.out.println(vetor[k].nome);
        }
        System.out.println();
    }
}